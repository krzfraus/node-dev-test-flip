import { Customer } from "./Customer"
import { Product } from "./Product"

export class Order {
    number: string
    customer: Customer
    products: Product[]
    createdAt: string

    constructor({ number, customer, products, createdAt }: { number: string, customer: Customer, products: Product[], createdAt: string }) {
        this.number = number
        this.customer = customer
        this.products = products
        this.createdAt = createdAt
    }

    get totalPrice() {
        return this.products.reduce((total, product) => {
            total += product.price
            return total
        }, 0)
    }
}
