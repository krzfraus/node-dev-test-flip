export class Product {
    name: string;
    price: number;

    constructor ({name, price}:{name: string, price: number}) {
        this.name = name;
        this.price = price;
    }
}
