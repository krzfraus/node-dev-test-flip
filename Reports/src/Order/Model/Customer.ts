export class Customer {
    firstName: string
    lastName: string

    constructor({ firstName, lastName }: { firstName: string, lastName: string }) {
        this.firstName = firstName
        this.lastName = lastName
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }
}
