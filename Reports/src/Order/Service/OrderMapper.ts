import { Injectable, Inject } from "@nestjs/common"
import { Repository } from "./Repository"
import { Order } from "../Model/Order"
import { Customer } from "../Model/Customer"
import { Product } from "../Model/Product"

@Injectable()
export class OrderMapper {
    @Inject() private repository: Repository

    public async fetchOrders(): Promise<Order[]> {
        const orders = await this.repository.fetchOrders()
        const customers = await this.repository.fetchCustomers()
        const products = await this.repository.fetchProducts()

        let customersMap = customers.reduce((map, customer) => {
            map[customer.id] = { firstName: customer.firstName, lastName: customer.lastName }
            return map
        }, {})
        let productsMap = products.reduce((map, product) => {
            map[product.id] = { name: product.name, price: product.price }
            return map
        }, {})

        return orders.reduce((arr, order, index) => {
            let customer = new Customer(customersMap[order.customer])
            let products = order.products.reduce((productsArray, productId, index) => {
                productsArray[index] = new Product(productsMap[productId])
                return productsArray
            }, [])
            arr[index] = new Order({ number: order.number, customer, products, createdAt: order.createdAt })
            return arr
        }, [])
    }

}
