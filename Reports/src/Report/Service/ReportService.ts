import { Injectable } from "@nestjs/common"
import { IBestBuyers, IBestSellers } from "../Model/IReports"
import { OrderMapper } from "../../Order/Service/OrderMapper"
import { loadConfigurationFromPath } from "tslint/lib/configuration"
import { createMatchPathAsync } from "tsconfig-paths"

@Injectable()
export class ReportService {
    constructor(private readonly orderMapper: OrderMapper) {
    }

    async getBestsellers({ date }: { date: string }): Promise<IBestSellers[]> {
        let bestsellers = []
        let productsInGivenDay = []
        let orders = await this.orderMapper.fetchOrders()

        let ordersInGivenDay = orders.filter(order => order.createdAt === date)
        if (ordersInGivenDay.length === 0) {
            return bestsellers
        }
        ordersInGivenDay.forEach((order) => {
            productsInGivenDay = productsInGivenDay.concat(order.products)
        })
        bestsellers = Object.values(productsInGivenDay.reduce((map, product) => {
            if (!map[product.name]) {
                map[product.name] = {
                    productName: product.name,
                    quantity: 1,
                    totalPrice: product.price,
                }
            } else {
                map[product.name].quantity++
                map[product.name].totalPrice += map[product.name].totalPrice
            }

            return map
        }, {})).sort((a: IBestSellers, b: IBestSellers) => {
            return b.quantity - a.quantity
        })
        return bestsellers
    }

    async getBestBuyers({ date }: { date: string }): Promise<IBestBuyers[]> {
        let bestBuyers = []
        let orders = await this.orderMapper.fetchOrders()

        let ordersInGivenDay = orders.filter(order => order.createdAt === date)
        if (ordersInGivenDay.length === 0) {
            return bestBuyers
        }
        bestBuyers = Object.values(ordersInGivenDay.reduce((map, order) => {
            let fullName = order.customer.fullName
            if (!map[fullName]) {
                map[fullName] = {
                    customerName: fullName,
                    totalPrice: order.totalPrice
                }
            } else {
                map[fullName].totalPrice += order.totalPrice
            }

            return map
        }, {})).sort((a: IBestBuyers, b: IBestBuyers) => {
            return b.totalPrice - a.totalPrice
        })
        return bestBuyers
    }
}
