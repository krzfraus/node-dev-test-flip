import { Controller, Get, HttpException, HttpStatus, Inject, Param } from "@nestjs/common"
import { IBestBuyers, IBestSellers } from "../Model/IReports"
import { ReportService } from "../Service/ReportService"
import * as moment from "moment"

@Controller()
export class ReportController {
    constructor(private readonly reportService: ReportService) {
    }

    @Get("/report/products/:date")
    async bestSellers(@Param("date") date: string): Promise<IBestSellers> {
        if (!moment(date, "YYYY-MM-DD", true).isValid()) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `Input date: ${date} seems not to be correct date. Please provide date in format: YYYY-MM-DD`,
            }, 400)
        }

        const dto = { date }
        let bestsellers = await this.reportService.getBestsellers(dto)

        if (bestsellers.length === 0) {
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: `There are no orders in a given date: ${date}. Nothing to return.`,
            }, 404)
        }

        return bestsellers[0]
    }

    @Get("/report/customer/:date")
    async bestBuyers(@Param("date") date: string): Promise<IBestBuyers> {
        if (!moment(date, "YYYY-MM-DD", true).isValid()) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `Input date: ${date} seems not to be correct date. Please provide date in format: YYYY-MM-DD`,
            }, 400)
        }

        const dto = { date }
        let bestBuyers = await this.reportService.getBestBuyers(dto)

        if (bestBuyers.length === 0) {
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: `There are no orders in a given date: ${date}. Nothing to return.`,
            }, 404)
        }

        return bestBuyers[0]
    }
}
