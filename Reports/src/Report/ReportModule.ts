import { Module } from '@nestjs/common';
import { ReportController } from './Controller/ReportController';
import { ReportService } from "./Service/ReportService"
import { OrderModule } from "../Order/OrderModule"

@Module({
  imports: [OrderModule],
  controllers: [ReportController],
  providers: [ReportService],
})
export class ReportModule {
}
