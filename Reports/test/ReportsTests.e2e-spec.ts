import * as request from "supertest"
import { Test } from "@nestjs/testing"
import { INestApplication } from "@nestjs/common"
import { ReportModule } from "./../src/Report/ReportModule"

describe("", () => {
    let app: INestApplication

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [ReportModule],
        }).compile()

        app = moduleFixture.createNestApplication()
        await app.init()
    })

    it("/ (GET)", () => {
        return request(app.getHttpServer())
            .get("/")
            .expect(404)
    })

    describe("GET /report/products/:date - get best sellers by date", () => {
        it("should return bestsellers", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/products/2019-08-07")

            expect(response.status).toBe(200)
            expect(response.body).toMatchObject({ productName: "Black sport shoes", quantity: 2, totalPrice: 220 })
        })

        it("should return bestsellers in sorted order", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/products/2019-08-08")

            expect(response.status).toBe(200)
            expect(response.body).toMatchObject({ productName: 'Blue jeans', quantity: 2, totalPrice: 111.98 })
        })

        it("should return 404 with error message due to lack of orders in given date", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/products/2011-08-07")
            expect(response.status).toBe(404)
        })

        it("should return 400 due to incorrect date", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/products/testData")
            expect(response.status).toBe(400)
        })

        it("should return 400 due to incorrect date format", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/products/11-11-2018")
            expect(response.status).toBe(400)
        })
    })

    describe("GET /report/customer/:date - get best buyer", () => {
        it("should return buyer with most money spent", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/customer/2019-08-07")

            expect(response.status).toBe(200)
            expect(response.body).toMatchObject({"customerName": "John Doe", "totalPrice": 135.75})
        })

        it("should return 404 with error message due to lack of orders in given date", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/customer/2011-08-07")
            expect(response.status).toBe(404)
        })

        it("should return 400 due to incorrect date", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/customer/testData")
            expect(response.status).toBe(400)
        })

        it("should return 400 due to incorrect date format", async () => {
            const response = await request(app.getHttpServer())
                .get("/report/customer/11-11-2018")
            expect(response.status).toBe(400)
        })
    })
})
