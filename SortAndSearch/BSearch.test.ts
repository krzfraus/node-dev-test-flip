import { BSearch } from "./BSearch"
import { elementsToFind } from "./index"


test("BSearch is a singleton", () => {
    let bSearch = BSearch.getInstance()
    let bSearch1 = BSearch.getInstance()
    expect(bSearch).toEqual(bSearch1)
})

test("BSearch should return index of element", () => {
    let bSearch = BSearch.getInstance()
    let res = bSearch.searchInArray(elementsToFind, 5)
    expect(bSearch.operations).toEqual(1)
    expect(res).toEqual(1)
})

test("BSearch should return -1", () => {
    let bSearch = BSearch.getInstance()
    let res = bSearch.searchInArray(elementsToFind, 99999)
    expect(res).toEqual(-1)
})

test("BSearch should return 2 for floats", () => {
    let bSearch = BSearch.getInstance()
    let res = bSearch.searchInArray([1.001, 1.01, 1.011, 1.012], 1.011)
    expect(res).toEqual(2)
})
