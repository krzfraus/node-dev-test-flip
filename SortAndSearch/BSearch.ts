export class BSearch {
    private static instance: BSearch

    private constructor() {
    }

    private _operations: number = 0

    get operations() {
        return this._operations
    }

    private incrementOperations() {
        this._operations++
    }

    public static getInstance(): BSearch {
        if (!this.instance) {
            this.instance = new BSearch()
        }
        return this.instance
    }

    public searchInArray(array: number[], search: number) {
        this.incrementOperations()
        return this.binarySearch(array, 0, array.length - 1, search)
    }

    private binarySearch(array: number[], low: number, high: number, search: number) {
        if (high >= low) {
            let mid = Math.ceil(low + (high - low) / 2)
            if (array[mid] == search) {
                return Math.floor(mid)
            }
            if (array[mid] > search) {
                return this.binarySearch(array, low, mid - 1, search)
            }
            return this.binarySearch(array, mid + 1, high, search)
        }
        return -1
    }
}
