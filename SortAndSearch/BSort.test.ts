import { BSort } from "./BSort"
import { unsorted } from "./index"

test("BSort should return sorted array - basic", () => {
    let res = BSort.sort(unsorted)
    expect(res).toEqual([0, 2, 4, 5, 7, 9, 13, 14, 17, 22, 32, 65, 77, 83])
})

test("BSort should return sorted array - resource contain negative integers", () => {
    let res = BSort.sort([10, -5, 0, 0, -15])
    expect(res).toEqual([-15, -5, 0, 0, 10])
})

test("BSort should return sorted array - resource contain floats", () => {
    let res = BSort.sort([1.0, 1.01, 1.1, 0.99])
    expect(res).toEqual([0.99, 1.0, 1.01, 1.1])
})

test("BSort should return empty array", () => {
    let res = BSort.sort([])
    expect(res).toEqual([])
})

test("BSort should return sorted array with only 2 elements", () => {
    let res = BSort.sort([2, 1])
    expect(res).toEqual([1, 2])
})

test("BSort should return array without sorting", () => {
    const spy = jest.spyOn(BSort, "setSwapped")
    let res = BSort.sort([1, 2, 3, 4, 5])
    expect(res).toEqual([1, 2, 3, 4, 5])
    expect(spy).toHaveBeenCalledTimes(1);
})
