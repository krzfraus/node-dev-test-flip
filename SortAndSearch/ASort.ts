export class ASort {
    public static sort(array: number[]): number[] {
        this.quickSort(array, 0, array.length - 1)
        return array
    }

    private static quickSort(array: number[], low: number, high: number) {
        if (low < high) {
            let pivotIndex = this.partition(array, low, high)
            this.quickSort(array, low, pivotIndex - 1)
            this.quickSort(array, pivotIndex + 1, high)
        }
    }

    private static partition(array: number[], low: number, high: number): number {
        let pivot: number = array[high]
        let i: number = (low - 1)
        for (let j = low; j < high; j++) {
            if (array[j] < pivot) {
                i++
                let temp: number = array[i]
                array[i] = array[j]
                array[j] = temp
            }
        }
        let temp: number = array[i + 1]
        array[i + 1] = array[high]
        array[high] = temp
        return i + 1
    }
}
