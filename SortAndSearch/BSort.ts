export class BSort {
    public static swapped: boolean

    public static setSwapped(val: boolean): void {
        this.swapped = val;
    }

    public static sort(array: number[]): number[] {
        this.bubbleSort(array)
        return array
    }

    private static bubbleSort(array: number[]) {
        let arrLength = array.length
        for (let i = 0; i < arrLength; i++) {
            this.setSwapped(false)
            for (let j = 0; j < arrLength - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    let temp = array[j]
                    array[j] = array[j + 1]
                    array[j + 1] = temp
                    this.setSwapped(true)
                }
            }
            if (this.swapped == false) {
                break
            }
        }
    }
}
