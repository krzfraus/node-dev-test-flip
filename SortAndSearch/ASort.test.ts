import { ASort } from "./ASort"
import { unsorted } from "./index"

test("ASort should return sorted array", () => {
    let res = ASort.sort(unsorted)
    expect(res).toEqual([0, 2, 4, 5, 7, 9, 13, 14, 17, 22, 32, 65, 77, 83])
})

test("ASort should return sorted array - resource contain negative integers", () => {
    let res = ASort.sort([10, -5, 0, 0, -15])
    expect(res).toEqual([-15, -5, 0, 0, 10])
})

test("ASort should return sorted array - resource contain floats", () => {
    let res = ASort.sort([1.0, 1.01, 1.1, 0.99])
    expect(res).toEqual([0.99, 1.0, 1.01, 1.1])
})

test("ASort should return empty array", () => {
    let res = ASort.sort([])
    expect(res).toEqual([])
})

test("ASort should return sorted array with only 2 elements", () => {
    let res = ASort.sort([2, 1])
    expect(res).toEqual([1, 2])
})
